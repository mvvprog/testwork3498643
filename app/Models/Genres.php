<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genres extends Model
{
    use HasFactory;

    protected $table = 'genres';
    protected $fillable = ['name', 'description', 'created_at', 'updated_at', 'degreeСonformity'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books()
    {
        $books = $this->belongsToMany(Books::class)->withPivot('degreeСonformity');
        return $books;
    }
}
