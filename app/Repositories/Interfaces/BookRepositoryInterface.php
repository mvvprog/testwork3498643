<?php

namespace App\Repositories\Interfaces;

use App\User;

interface BookRepositoryInterface
{
    public function all();

    public function getById(int $id);

    public function findByCriteria(array $criteria, array $relations = []);
}
