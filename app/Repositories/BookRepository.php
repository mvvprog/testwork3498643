<?php
namespace App\Repositories;
use App\Models\Books;
use App\Repositories\Interfaces\BookRepositoryInterface;

class BookRepository implements BookRepositoryInterface
{
    public function all()
    {
        return Books::with('genres')->orderBy('name')->get();
    }

    public function getById(int $id)
    {
        return Books::with('genres')->find($id);
    }

    public function findByCriteria(array $criteria, array $relations = [])
    {
        return Books::with($relations)->where($criteria)->get();
    }
}
