<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GenreBookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $orig = $this->getOriginal();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'degreeСonformity' => $orig['pivot_degreeСonformity'],
        ];
    }
}
