<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth_api'], function () {
    Route::apiResources([
        'books' => BookController::class,
        'genres' => GenreController::class,
    ]);
});

Route::get('searchbook', [App\Http\Controllers\Api\V1\BookController::class, 'search'])->middleware('auth_api');
